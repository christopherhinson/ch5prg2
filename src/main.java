import java.util.Scanner;

public class main {
    public static void main(String args[])
    {
        Output o = new Output();

        double finalCost = calulateRetail(getWholeSale());

        o.print("Total Cost :","Purple");
        o.print(finalCost, "Red");
    }

    public static double getWholeSale()
    {
        Output o = new Output();
        double wholeSaleCost;
        Scanner k = new Scanner(System.in);

        o.print("Please enter the item's wholesale cost","Purple");
        wholeSaleCost = k.nextDouble();

        return wholeSaleCost;
    }

    public static double calulateRetail(double wholesale)
    {
        Scanner k = new Scanner(System.in);
        Output o = new Output();
        double markUp;
        double retailCost;

        o.print("Please enter the markup percentage", "Purple");

        markUp = k.nextDouble();

        retailCost = wholesale + wholesale*(markUp/100);

        return retailCost;
    }
}
